﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2Words
{
    interface ICountVowels
    {
        int GetVowelsCount();
    }

    interface ICountConsonants
    {
        int GetConsonantsCount();
    }



/// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    abstract class Word : ICountVowels, ICountConsonants
    {
        private string term;
        public Word(string term)
        {
            this.term = term;
        }


        public string Term
        {
            get { return term; }
        }

        public int GetVowelsCount()
        {
            int numberOfVowels = 0;

            foreach (char c in term)
            {
                if ((c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u'))
                {
                    numberOfVowels++;
                }
                else
                {
                    continue;
                }
            }
            return numberOfVowels;
        }

        public int GetConsonantsCount()
        {
        
            int numberOfvowels = GetVowelsCount();
            int charCounter = 0;
            int numberOfConsonants = 0;

            foreach (char c in term)
            {
                charCounter++;
            }

            numberOfConsonants = charCounter - numberOfvowels;

            return numberOfConsonants;
        }

        public override string ToString()
        {
            return String.Format("Word: {0} - has {1} vowels and {2} consonants.", term, GetVowelsCount(), GetConsonantsCount());
        }
    }


    /// ////////////////////////////////////////////////////////////////////////////////////////////

    class Verb : Word
    {
        private string term;
        public Verb(string term) : base(term)
        {
            this.term = term;
        }
        public override string ToString() 
        {
            return String.Format("Verb: {0} - has {1} vowels and {2} consonants.", Term, GetVowelsCount(), GetConsonantsCount());
        }

            
    }


    /// ///////////////////////////////////////////////////////////////////////////////////////////

    class Noun : Word
    {
        private string term;
        public Noun(string term) : base(term)
        {
            this.term = term;
        }
        public override string ToString() 
        {
            return String.Format("Noun: {0} - has {1} vowels and {2} consonants.", Term, GetVowelsCount(), GetConsonantsCount());
        }            
    }

    /// //////////////////////////////////////////////////////////////////////////////////////////////

    class Other : Word
    {
        private string term;
        public Other(string term) : base(term)
        {
            this.term = term;
        }

        public override string ToString() 
        {
            return String.Format("Other: {0} - has {1} vowels and {2} consonants.", Term, GetVowelsCount(), GetConsonantsCount());
        }            
    }

/// /////////////////////////////////////////////////////////////////////////////////////////////////////

    class WordByWeightComparer : IComparer<Word>
    {
        public int Compare(Word x, Word y)
        {
            return ((4 * x.GetVowelsCount()) + x.GetConsonantsCount()).CompareTo((4 * y.GetVowelsCount()) + y.GetConsonantsCount());

        }
    }


/// ////////////////////////////////////////////////////////////////////////////////////////////////////

    class Program
    {
        static void Main(string[] args)
        {
            List <Word> wordList = new List <Word> ();

            Verb v1 = new Verb("dance");
            Verb v2 = new Verb("create");
            Verb v3 = new Verb("throw");
            Noun n1 = new Noun("phone");
            Noun n2 = new Noun("wire");
            Noun n3 = new Noun("screen");
            Other o1 = new Other("very");
            Other o2 = new Other("tellement");
            Other o3 = new Other("ordinairement");

            wordList.Add(v1);
            wordList.Add(v2);
            wordList.Add(v3);
            wordList.Add(n1);
            wordList.Add(n2);
            wordList.Add(n3);
            wordList.Add(o1);
            wordList.Add(o2);
            wordList.Add(o3);

            foreach (Word w in wordList)
            {
                Console.WriteLine(w);
            }


           wordList.Sort(new WordByWeightComparer ());

           Console.WriteLine("******************Sorted by weight************************");

            foreach (Word w in wordList)
            {
                Console.WriteLine(w);
            }
            

            Console.ReadKey();

        }
    }
}
